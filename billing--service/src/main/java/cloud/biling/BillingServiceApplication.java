package cloud.biling;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class Bill {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date date;
	private Long customerId;
	@OneToMany(mappedBy = "bill")
	private Collection<ProductItem> productItems;
}

@RepositoryRestResource
interface Billrepository extends JpaRepository<Bill, Long> {

}

@Projection(name = "fullBill", types = Bill.class)
interface BillProjection {
	public Long getId();

	public Date getDate();

	public Long getCustomerId();

	public Collection<ProductItem> getProductItems();
}

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class ProductItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long productId;
	private double price;
	private double quantite;
	@ManyToOne
	private Bill bill;
}

@RepositoryRestResource
interface ProductItemRepository extends JpaRepository<ProductItem, Long> {

}

// Interface declarative pour recuperer un client du service CUSTOMER-SERVICE
@FeignClient(name = "CUSTOMER-SERVICE")
interface CustomerService {
	@GetMapping("/customers/{id}")
	public Customer findByCustomerId(@PathVariable(name = "id") Long id);
}

@Data
class Customer {
	private Long id;
	private String name;
	private String email;
}

@Data
class Product{
	private Long id ;
	private String name ;
	private double price ;
}

@FeignClient(name = "INVENTORY-SERVICE")
interface ProductService{
	@GetMapping("/produts/{id}")
	public Product findByProductId(@PathVariable(name = "id") Long id);
}

@SpringBootApplication
@EnableFeignClients
public class BillingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillingServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(Billrepository billrepository, ProductItemRepository productItemRepository,
			CustomerService customerService, ProductService productService) {
		return args -> {
		    Customer customer1 = customerService.findByCustomerId(1L);
		    System.out.println("INFORMATIONS DU CLIENT -----------------");
		    System.out.println("ID :: "+customer1.getId());
		    System.out.println("Name :: "+customer1.getName());
		    System.out.println("Email :: "+customer1.getEmail());
            System.out.println(" IS ALL  ++++++++++++++++++++");
			/*
			 * Bill bill1 = billrepository.save(new Bill(null, new Date(),
			 * customer1.getId(), null)); Product product =
			 * productService.findByProductId(1L);
			 * System.out.println("----INFORMATIONS SUR LE PRODUIT-----------");
			 * System.out.println("ID ::::"+product.getId());
			 * System.out.println("NAME PRODUCT "+product.getName());
			 * System.out.println("PRIX PRODUIT"+product.getPrice());
			 * productItemRepository.save(new ProductItem(null, 1L, 800.0, 30, bill1));
			 * productItemRepository.save(new ProductItem(null, 2L, 750.0, 30, bill1));
			 * productItemRepository.save(new ProductItem(null, 4L, 965.0, 30, bill1));
			 */

		};
	}
}
