package cloud;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.config.Projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class Customer{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id ;
	private String name ;
	private String email ;
}

@RepositoryRestResource
interface CustomerRepository extends JpaRepository<Customer, Long> {
	
};

@Projection(name = "p1", types = Customer.class)
interface CustomerProjection{
	public Long getId();
	public String getName();
	public String getEmail();
	
}

@SpringBootApplication
public class CustomerServiceApplication implements CommandLineRunner{

	@Autowired
	private CustomerRepository customerRepository ;
	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		customerRepository.save(new Customer(null, "Moussa Ndiaye", "mouissalmith@gmail.com"));
		customerRepository.save(new Customer(null, "Abdoulaye Ndiaye", "ndiayemoussa@gmail.com"));

	}

}
