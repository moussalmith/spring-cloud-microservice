package cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@SpringBootApplication
public class GatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayServiceApplication.class, args);
	}

	

/*	  @Bean
	  RouteLocator staticRoute(RouteLocatorBuilder builder) {
		  return builder.routes()
				  .route(r -> r.path("/covid-19/**")
						  .filters(f -> f.addRequestHeader("x-rapidapi-host", "covid-193.p.rapidapi.com")
								  .addRequestHeader("x-rapidapi-key", "bf1cb4624dmsh2efd132ba280c1ap1cf502jsndcb5f15ca65f")
								  .rewritePath("/static-covid-19/<?segment>.*", "${segment}")
								  .hystrix(h -> h.setName("rest-covid-19")
										  .setFallbackUri("rest-covid-FailBack-Default"))
						  )
						  .uri("https://montanaflynn-fifa-world-cup.p.rapidapi.com/teams")
						  .id("world-cup"))
				  .route(r -> r.path("/data-sport/**")
						  .filters(f -> f.addRequestHeader("x-rapidapi-host",  "sport-data.p.rapidapi.com")
								  .addRequestHeader("x-rapidapi-key", "bf1cb4624dmsh2efd132ba280c1ap1cf502jsndcb5f15ca65f")
								  .rewritePath("/sport-data/<?segment>.*", "${segment}")
								  .hystrix(h -> h.setName("sport")
										  .setFallbackUri("forward/rest-sport-FailBack-Default"))
						  )
						  .uri("https://sport-data.p.rapidapi.com/api/listBetMarkets//")
						  .id("world-cup"))
				  .build();
	  }*/


				    //
					//		  .uri("http://8081/").id("r1")) .route(r ->
	                //  r.path("/products/**").uri("http://8082/").id("r2")) .build(); }

	 
	/**
	 * Configurer les routes de manieres dynamiques
	 * @param client
	 * @param properties
	 * @return
	 */
	
	@Bean
	DiscoveryClientRouteDefinitionLocator dynamicRoutes(ReactiveDiscoveryClient discoveryClient, DiscoveryLocatorProperties properties) {
		return new DiscoveryClientRouteDefinitionLocator(discoveryClient, properties);
	}
}
